﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_5
{
    public class Excalibur : Weapon
    {
        public Excalibur()
        {
            Name = "Excalibur";
            Damage = 3000;
        }
    }
}
