﻿using System;

namespace Day_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Sword Art Online");
            Console.Write("Please Input your name :");
            Greeting name = new Greeting();
            name.greet(Console.ReadLine());

            Weapon dualSword = new DualSword();
            Weapon rapier = new Rapier();
            Weapon excalibur = new Excalibur();
            Console.WriteLine("Please choose your weapon : ");
            Console.WriteLine("1." + dualSword.Name + " " + "2." + rapier.Name + " " + "3." + excalibur.Name);
            string chooseWeapon = Console.ReadLine();
 
            while(chooseWeapon != "1" && chooseWeapon != "2" && chooseWeapon !="3")
            {
                Console.Write("Please input with valid number (1-3) : ");
                chooseWeapon = Console.ReadLine();
            }

            Console.WriteLine("You Choose " + chooseWeapon);
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            switch (chooseWeapon)
            {
                case "1":
                    Console.WriteLine("Your weapon is " + dualSword.Name + " And your damage " + dualSword.Damage);
                    Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    Console.WriteLine("Welcome to the world " + name.playerName + " you had been equip with " + dualSword.Damage);
                    Console.WriteLine("Pikachu appear with 10000 hp " + "and you had defeat with " + dualSword.dealDamage() + " turns" );
                  
                    break;
                case "2":
                    Console.WriteLine("You weapon is " + rapier.Name + " And your damage " + rapier.Damage);
                    Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    Console.WriteLine("Welcome to the world " + name.playerName + " you had been equip with " + rapier.Damage);
                    Console.WriteLine("Pikachu appear with 10000 hp " + "and you had defeat with " + rapier.dealDamage() + " turns");
                    break;
                case "3":
                    Console.WriteLine("You weapon is " + excalibur.Name + " And your damage " + excalibur.Damage);
                    Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    Console.WriteLine("Welcome to the world " + name.playerName + " you had been equip with " + excalibur.Damage);
                    Console.WriteLine("Pikachu appear with 10000 hp " + "and you had defeat with " + excalibur.dealDamage() + " turns");
                    break;
                default:
                    Console.WriteLine("Invalid Number");
                    break;
            }
        }
    }
}
