﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_5
{
    public class Rapier : Weapon
    {
        public Rapier()
        {
            Name = "Rapier";
            Damage = 1000;
        }
    }
}
