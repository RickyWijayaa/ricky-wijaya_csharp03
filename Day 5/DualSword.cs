﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_5
{
    public class DualSword : Weapon
    {
        public DualSword()
        {
            Name = "DualSword";
            Damage = 2000;
        }
    }
}
