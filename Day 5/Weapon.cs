﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_5
{
    public abstract class Weapon
    {
        public string Name { get; set; }
        public int Damage { get; set; }


        public int dealDamage(int hp = 10000)
        {
           return hp / this.Damage;
        }
    }
}
